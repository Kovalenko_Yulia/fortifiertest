var filmsLibrary = [
  {
    image: 'https://www.film.ru/sites/default/files/styles/thumb_260x400/public/movies/posters/32207910-1030383.jpg',
    title: 'Bad Samaritan',
    description: 'В цeнтрe сюжeтa окaзывaются двоe молодых людeй, которыe используют свой aвтомобильный бизнeс кaк прикрытиe. Нa сaмом дeлe молодыe люди зaрaбaтывaют нa жизнь, грaбя домa своих жe клиeнтов. Однaжды, во врeмя очeрeдной вылaзки, глaвныe гeрои попaдaют в нeпрeдвидeнную ситуaцию, когдa бeрут в зaложницы жeну влaдeльцa домa.',
    country: 'USA',
    year: '2018',
    genre: 'триллер / ужасы',
    actors: 'Aksel Hennie, Chiwetel Ejiofor, Jeff Daniels, Jessica Chastain, Kate Mara, Kristen Wiig, Matt Damon, Michael Peña, Sean Bean, Sebastian Stan',
    comments: []
  },
  {
    image: 'https://www.film.ru/sites/default/files/styles/thumb_260x400/public/movies/posters/31549052-1034574.jpg',
    title: 'Truth or Dare',
    description: 'Группа друзей-студентов собирается отметить успешное завершение сессии, отправившись на отдых в загородный особняк. Там ребята решают развлечь себя вполне безобидной игрой «Правда или Желание», где перед участниками ставится непростой выбор ‒ сказать правду или исполнить желание вопрошающего. Однако вскоре весёлое занятие оборачивается для студентов настоящим кошмаром: если кто-то из них скажет неправду или не исполнит требуемое желание, то игра жестоко накажет его.',
    country: 'USA',
    year: '2018',
    genre: 'триллер / ужасы',
    actors: 'Aksel Hennie, Chiwetel Ejiofor, Jeff Daniels, Jessica Chastain, Kate Mara, Kristen Wiig, Matt Damon, Michael Peña, Sean Bean, Sebastian Stan',
    comments: []
  },
  {
    image: 'http://gatchina-news.ru/files/images/afisha/movie/108_n38321602.jpg',
    title: 'Martian',
    description: 'Mars mission Ares-2 in the process was forced to urgently leave the planet because of the impending sandstorm. Engineer and Biolog mark Watney suffered an injury suit during a sand storm. The mission, considering him dead...',
    country: 'USA',
    year: '2016',
    genre: 'Fantasy',
    actors: 'Aksel Hennie, Chiwetel Ejiofor, Jeff Daniels, Jessica Chastain, Kate Mara, Kristen Wiig, Matt Damon, Michael Peña, Sean Bean, Sebastian Stan',
    comments: [
        'Test comment 1',
        'Test comment 2'
    ]
  },
  {
    image: 'https://resizing.flixster.com/eip8Oek1zHyeV6-hRtkai1ve8Ck=/206x305/v1.bTsxMjU4OTA5NztqOzE3Njk1OzEyMDA7MTY4ODsyNTAw',
    title: 'A WRINKLE IN TIME',
    description: 'Following the discovery of a new form of space travel as well as Meg\'s father\'s disappearance, she, her brother, and her friend must join three magical beings - Mrs. Whatsit, Mrs. Who, and Mrs. Which - to travel across the universe to rescue him from a terrible evil.',
    country: 'USA',
    year: '2018',
    genre: 'Adventure, Family, Fantasy, Sci-Fi',
    actors: 'Storm Reid, Oprah Winfrey, Reese Witherspoon, Mindy Kaling, Levi Miller, Deric McCabe, Chris Pine, Gugu Mbatha-Raw, Zach Galifianakis, Michael Peña, André Holland, Rowan Blanchard',
    comments: [
        'Test comment 1',
        'Test comment 2',
        'Test comment 3'
    ]
  },
  {
    image: 'https://resizing.flixster.com/Aw5erugAHEB-XKIr7UCKtwuSdZk=/206x305/v1.bTsxMjU4NDE3OTtqOzE3Njk1OzEyMDA7Mjc2NDs0MDk2',
    title: 'RAMPAGE',
    description: 'Athena-1, a research space station owned by gene manipulation company Energyne, is destroyed after a laboratory rat mutates and wreaks havoc. Dr. Kerry Atkins, the lone surviving crew member, is ordered to retrieve research canisters containing a pathogen by CEO Claire Wyden. Atkins is able to flee in the escape pod, but it disintegrates upon re-entry, killing her, and leaving a trail of debris across the United States, including the Everglades, where a canister is consumed by an American crocodile, and a forest in Wyoming, where a gray wolf is exposed to the pathogen.\r\nPrimatologist Davis Okoye, a former US Army Special Forces soldier and member of an anti-poaching unit, works at a San Diego wildlife preserve. He befriends a rare albino gorilla named George, whom he saved from poachers. One night, one of the canisters crash-lands in George\'s habitat and George is exposed to it.\r\nGeorge grows considerably larger and aggressive overtime. Davis is contacted by Dr. Kate Caldwell, a genetic engineer, who explains that the pathogen was developed by Energyne to rewrite genes on a massive scale. She had hoped to advance CRISPR research as a potential cure for disease but discovered that Energyne planned to use it as a biological weapon. The company fired her and got her sent to prison for attempting to expose them. George escapes from captivity and goes on a rampage at the preserve. Davis calms him down, but George is captured by a government team led by Agent Harvey Russell, and is put on an airplane. Meanwhile, Claire and her brother, Brett, send a squad of mercenaries led by Burke to capture the mutated wolf, Ralph, but they are all killed.',
    country: 'USA',
    year: '2018',
    genre: 'Fantasy',
    actors: 'Aksel Hennie, Chiwetel Ejiofor, Jeff Daniels, Jessica Chastain, Kate Mara, Kristen Wiig, Matt Damon, Michael Peña, Sean Bean, Sebastian Stan',
    comments: [
        'Test comment 1',
        'Test comment 2',
        'Test comment 3',
        'Test comment 4'
    ]
  }
];


var LibraryStorage = JSON.parse(localStorage.getItem('filmsLibrary'));

$( document ).ready(function() {
    if (LibraryStorage !== null){
        filmsLibrary = LibraryStorage;
    }
    for ( var i = 0; i < filmsLibrary.length; i++){
        if (filmsLibrary[i]['comments'].length){
            var comments = '<p class="amount">Comments: <span>' + filmsLibrary[i]['comments'].length + '</span><i class="fa fa-chevron-up" aria-hidden="true"></i></p>';
            for (var j = filmsLibrary[i]['comments'].length - 1; j > -1; j--){
                comments = comments + '<p class="comment">' + filmsLibrary[i]['comments'][j] + '</p>'
            }
        }else{
            comments = '<p class="amount">Comments: <span>0</span><i class="fa fa-chevron-up" aria-hidden="true"></i></p>';
        }
        BuildPage(i, comments);
    }
});

$( document ).ready(function() {
    //delete film item
    $(document).on("click", ".film-item .delete", function(e) {
        e.preventDefault();
        var id = $(this).parents('.film-item').attr('id');
        id = id.replace('item-', '');

        $('#myModal').find('img').attr('src', filmsLibrary[id]['image']);
        $('#myModal').attr('data',id);
    });

    $(document).on('click', '#myModal .cancel', function(e){
        e.preventDefault();
        $(function () {
            $('#myModal').modal('hide');
        });
    });

    $( document ).on('click', '#myModal .delete', function(e){
            e.preventDefault();
            $(function () {

                var id = $('#myModal').attr('data');
                $('#item-' + id).remove();
                filmsLibrary.splice(id, 1);

                $('#myModal').modal('hide');

                localStorage.setItem('filmsLibrary', JSON.stringify(filmsLibrary));

                //show preloader, delete all films
                $('#page-preloader').removeClass('done');
                $(".content").html('');

                //add all film again
                for ( var i = 0; i < filmsLibrary.length; i++){

                    if (filmsLibrary[i]['comments'].length){
                        var comments = '<p class="amount">Comments: <span>' + filmsLibrary[i]['comments'].length + '</span><i class="fa fa-chevron-up" aria-hidden="true"></i></p>';
                        for (var j = filmsLibrary[i]['comments'].length - 1; j > -1; j--){
                            comments = comments + '<p class="comment">' + filmsLibrary[i]['comments'][j] + '</p>'
                        }
                    }else {
                        comments = '<p class="amount">Comments: <span>0</span><i class="fa fa-chevron-up" aria-hidden="true"></i></p>';
                    }
                    BuildPage(i, comments);
                }
                //hide preloader
                setTimeout(function(){
                    $('#page-preloader').addClass('done');
                }, 1500);

            });
        });

    //edit film item content
    $(document).on("click", ".film-item .edit", function(e) {
        e.preventDefault();
        $(this).parents('.film-item').addClass('active');
        $(this).parents('.film-item').find('.poster').addClass('active');
        $(this).parents('.film-item').find('input').each(function() {
            $(this).prop( "disabled", false );
        });
        $(this).parents('.film-item').find('textarea').each(function() {
            $(this).prop( "disabled", false );
        });
        $(this).html('save');
        $(this).attr('class','save');
        $(this).addClass('active');
    });

    //save film item content changes
    $(document).on("click", ".film-item .save", function(e) {
        e.preventDefault();
        $(this).parents('.film-item').removeClass('active');
        $(this).parents('.film-item').find('.poster').removeClass('active');
        $(this).parents('.film-item .film-content').find('input').each(function() {
            $(this).prop( "disabled", true );
        });
        $(this).parents('.film-item .film-content').find('textarea').each(function() {
            $(this).prop( "disabled", true );
        });
        $(this).html('edit');
        $(this).attr('class','edit');
        $(this).removeClass('active');
    });

    //open comments block
    $(document).on("click", ".comments .amount", function() {
        $(this).parent('.comments').toggleClass('active');
        var commentsHeight = $(this).parent('.comments').outerHeight();

        if ($(this).parent('.comments').hasClass('active')){
            var itemHeight = 600 + commentsHeight + 'px';

            $(this).parent().parent('.film-item').css('height', itemHeight);
        } else{
            $(this).parent().parent('.film-item').css('height', '700px');

            $(this).parent().parent('.film-item').find('.comments div').removeClass('unvalid');
            $(this).parent().parent('.film-item').find('.comments div').addClass('valid');
        }
    });

    //add new comment
    $(document).on("click", ".comments .add-com", function(e) {
        e.preventDefault();
        var input = $(this).parent('.comments').find('input').val();
        var id = $(this).parents('.film-item').attr('id');
        id = id.replace('item-', '');

        if (!input){
            $(this).parent('.comments').find('div').addClass('unvalid');
        } else {
            //add new comment and change height of comments block
            var blockHeight = $(this).parent().parent('.film-item').outerHeight();
            $(this).parent('.comments').find('p.amount').after('<p class="comment">' + input + '</p>');
            var itemHeight = $(this).parent('.comments').find('p.comment').last().outerHeight();
            var totalHeight = blockHeight + itemHeight;
            $(this).parent().parent('.film-item').css('height', totalHeight);
            //change total comments amount
            var commentAmount = $(this).parent('.comments').find('.comment').length;
            $(this).parent('.comments').find('p.amount span').html(commentAmount);

            //save new comment to object
            filmsLibrary[id]['comments'][commentAmount-1] = input;

            $(this).parent('.comments').find('input').val('');
        }

        localStorage.setItem('filmsLibrary', JSON.stringify(filmsLibrary));
    });

    //remove unvalid message in comments block on input focus
    $(document).on('focus click', 'input',  function(){
        if($(this).next('span')){
            $(this).parent('div.field').removeClass('unvalid');
            $(this).parent('div.field').addClass('valid');
        }
    });
    //remove unvalid message in comments block on textarea focus
    $(document).on('focus click', 'textarea',  function(){
        if($(this).next('span')){
            $(this).parent('div.field').removeClass('unvalid');
            $(this).parent('div.field').addClass('valid');
        }
    });

    //save changes to object if input value was changed
    $(document).on("change", ".film-item input", function() {

        var id = $(this).parents('.film-item').attr('id');
        id = id.replace('item-', '');
        var className = $(this).attr('class');
        var value = $(this).val();

        if (className!='add-comment'){
            filmsLibrary[id][className] = value;
        }

        localStorage.setItem('filmsLibrary', JSON.stringify(filmsLibrary));
    });

    //save changes to object if textarea value was changed
    $(document).on("change", ".film-item textarea", function() {

        var id = $(this).parents('.film-item').attr('id');
        id = id.replace('item-', '');
        var className = $(this).attr('class');
        var value = $(this).val();

        filmsLibrary[id][className] = value;
        localStorage.setItem('filmsLibrary', JSON.stringify(filmsLibrary));

        if (className == 'image'){
            $(this).parents('.film-item').find('.poster img').attr('src', value);
            $(this).parents('.film-item').find('.film-image img').attr('src', value);
        }
    });

    //show "add new item" block
    $(document).on("click", ".addNewFilm p.title", function() {
        $(this).parents('.addNewFilm').toggleClass('active');

        if ($(this).parents('.addNewFilm').hasClass('active')){
            $(this).parents('body').find('.content').addClass('active');
        } else{
            $(this).parents('body').find('.content').removeClass('active');
        }
    });
    $(document).on("click", ".addNewFilm div.plus", function() {
        $(this).parents('.addNewFilm').toggleClass('active');

        if ($(this).parents('.addNewFilm').hasClass('active')){
            $(this).parents('body').find('.content').addClass('active');
        } else{
            $(this).parents('body').find('.content').removeClass('active');
        }
    });

    //hide "add new item" block on 'cancel' click
    $(document).on("click", ".addNewFilm .add .cancel", function(e) {
        e.preventDefault();
        $(this).parents('body').find('.content').removeClass('active');
        $(this).parents('.addNewFilm').removeClass('active');
        $(this).parents('.addNewFilm').find('input').each(function(){
            $(this).val('');
        });
        $(this).parents('.addNewFilm').find('textarea').val('');
    });

    //add new film item
    $(document).on("click", ".addNewFilm .add .add-new", function(e) {
        e.preventDefault();
        var length = filmsLibrary.length;

        var obj ={
            image: '',
            title: '',
            description: '',
            country: '',
            year: '',
            genre: ''
          };

        $(this).parents('.addNewFilm').find('input').each(function(){
            if (!$(this).val()){
                $(this).parents('.field').addClass('unvalid');
            }else{
                var className = $(this).attr('class');
                obj[className] = $(this).val();
            }
        });
        var textarea = $(this).parents('.addNewFilm').find('textarea');
        if (!textarea.val()){
            textarea.parents('.field').addClass('unvalid');
        }else{
            var className = textarea.attr('class');
            obj[className] = textarea.val();
        }

        for(var key in obj){
            if (obj[key] == ''){
                return false;
            } else{
                filmsLibrary[length] = {
                    image: obj['image'],
                    title: obj['title'],
                    description: obj['description'],
                    country: obj['country'],
                    year: obj['year'],
                    genre: obj['genre'],
                    actors: '',
                    comments: []
                  };
            }
        }

        localStorage.setItem('filmsLibrary', JSON.stringify(filmsLibrary));

        $(this).parents('.addNewFilm').find('input').each(function(){
        $(this).val('');
        });
        $(this).parents('.addNewFilm').find('textarea').val('');
        $('body').find('.content').removeClass('active');
        $('.addNewFilm').removeClass('active');
        var comments = '<p class="amount">Comments: <span>0</span><i class="fa fa-chevron-up" aria-hidden="true"></i></p>';

        BuildPage(length, comments);

    });

});

function BuildPage(i, comments){
    $(".content").prepend('<div class="film-item" id="item-' + i + '"><div class="film-image"><img src="' + filmsLibrary[i]['image'] + '" alt=""></div><div class="film-content"><form action=""><div class="poster"><img src="' + filmsLibrary[i]['image'] + '" alt=""><textarea name="poster" class="image" disabled="">' + filmsLibrary[i]['image'] + '</textarea></div><div class="text"><input type="text" class="title" value="' + filmsLibrary[i]['title'] + '" disabled=""><textarea name="description" class="description" placeholder="" disabled="">' + filmsLibrary[i]['description'] + '</textarea><div class="lables"><p class="country">country</p><input type="text" class="country" value="' + filmsLibrary[i]['country'] + '" disabled=""><p class="year">year</p><input type="text" class="year" value="' + filmsLibrary[i]['year'] + '" disabled=""><p class="genre">genre</p><input type="text" class="genre" value="' + filmsLibrary[i]['genre'] + '" disabled=""><p class="actors">actors</p><textarea type="text" class="actors" placeholder="" disabled="">' + filmsLibrary[i]['actors'] + '</textarea></div><div class="buttons"><a href="#" class="delete" data-toggle="modal" data-target="#myModal">delete</a><a href="#" class="edit">edit</a></div></div></form></div><div class="comments">' + comments + '<div class="field"><input type="text" class="add-comment" placeholder="Add your comment"><span>This field can\'t be empty</span></div><a href="#" class="add-com">Add</a></div></div>');
}